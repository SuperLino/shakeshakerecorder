package com.gdkdemo.ShakeShakeRecorder;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.Locale;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.WindowManager;
import android.widget.TextView;

public class MainActivity extends Activity {

	private TextView tv_ScreenContent;
	private File recordeFile;

	// 声明媒体录音机和媒体播放器对象
	private MediaRecorder mMediaRecorder;
	private MediaPlayer mMediaPlayer;

	SensorManager mSensorManager;
	// 定义传感器事件监听器
	SensorEventListener acceleromererListener;
	Sensor acceleromererSensor;

	// 记录上次触发时间和当前时间
	private long lastTime = 0;
	private long curTime = 0;
	// 是否正在录音
	private boolean isRecording = false;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);// 屏幕保持常亮
		setContentView(R.layout.activity_main);// 获取传感器管理器

		tv_ScreenContent = (TextView) findViewById(R.id.screen_content);
		mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		// 获取加速度传感器
		acceleromererSensor = mSensorManager
				.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

		// 定义传感器事件监听器
		acceleromererListener = new SensorEventListener() {

			@Override
			public void onAccuracyChanged(Sensor sensor, int accuracy) {
				// 什么也不干
			}

			// 传感器数据变动事件
			@Override
			public void onSensorChanged(SensorEvent event) {

				// 获得传感器类型
				int sensorType = event.sensor.getType();

				// values[0]:X轴，values[1]：Y轴，values[2]：Z轴
				float[] values = event.values;

				if (sensorType == Sensor.TYPE_ACCELEROMETER) {

					curTime = System.currentTimeMillis();

					// xyz轴任意一个方向的加速度大于15且距离上次触发时间大于500毫秒，触发事件
					if (((Math.abs(values[0]) > 15 || Math.abs(values[1]) > 15 || Math
							.abs(values[2]) > 15))
							&& (curTime - lastTime) > 500) {
						lastTime = curTime;
						switchRecord();
					}
				}

			}

		};

	}

	protected void onResume() {
		super.onResume();
		// 在传感器管理器中注册监听器
		mSensorManager.registerListener(acceleromererListener,
				acceleromererSensor, SensorManager.SENSOR_DELAY_UI);
	}

	protected void onPause() {
		mSensorManager.unregisterListener(acceleromererListener);
		mMediaPlayer.release();
		super.onPause();
	}

	protected void switchRecord() {
		Log.e("switchRecord", "1");
		if (!isRecording) {
			recordeFile = new File("/sdcard/"
					+ new DateFormat().format("yyyyMMdd_hhmmss",
							Calendar.getInstance(Locale.CHINA)) + ".aac");
			tv_ScreenContent.setText("正在录音，录音文件在"
					+ recordeFile.getAbsolutePath());

			initMediaRecorder();

			try {
				// 创建文件
				recordeFile.createNewFile();
				// 准备录制
				mMediaRecorder.prepare();
				mMediaRecorder.start();
			} catch (IllegalStateException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			// 开始录制
			tv_ScreenContent.setText("录音中……");
			isRecording = true;
		} else {
			if (mMediaRecorder != null) {
				mMediaRecorder.stop();
				mMediaRecorder.release();
				mMediaRecorder = null;
				tv_ScreenContent.setText("录音完毕");

				mMediaPlayer = new MediaPlayer();
				mMediaPlayer.reset();
				try {
					Log.e("setDataSource", recordeFile.getAbsolutePath());
					mMediaPlayer.setDataSource(recordeFile.getAbsolutePath());
					mMediaPlayer.prepare();
					mMediaPlayer.start();

				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (SecurityException e) {
					e.printStackTrace();
				} catch (IllegalStateException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}

				isRecording = false;
			}
		}

	}

	public void initMediaRecorder() {

		if (mMediaRecorder == null) {
			// 创建录音对象
			mMediaRecorder = new MediaRecorder();

			// 从麦克风源进行录音
			mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.DEFAULT);

			// 设置输出格式
			mMediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.DEFAULT);

			// 设置编码格式，必须为AAC
			mMediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);

			// 设置输出文件
			mMediaRecorder.setOutputFile(recordeFile.getAbsolutePath());
		}
	}

}